% Adam Miller - D

numCorrect= 0;
totalNum= 100000;
for x=1:totalNum
    numCorrect= numCorrect + (x .* (1./x) == 1);
end
correctRate= numCorrect * 100 / totalNum;

fprintf('Number of integers that satisfy the identity: %d\n', numCorrect);
fprintf('Percentage: %g\n', correctRate);
