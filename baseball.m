%Adam Miller - C
g = 9.8;
Vo = 43.81;
time = linspace(0, 2*Vo/g, 25);
for x=1:25
    t = time(x);
    height(x) = Vo*t - g*(t^2) / 2;
end

disp([time',height']);
maxHeight = max(height);
fprintf("\nMax Height = %f", maxHeight);